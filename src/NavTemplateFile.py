"""
Represents a navigation template file

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

import re
from HtmlTemplateFile import HtmlTemplateFile


class NavTemplateFile(HtmlTemplateFile):

    def __init__(self, outputDir, contentPath, title):
        super(NavTemplateFile, self).__init__(outputDir, contentPath, title)
        self.outputFilename = re.sub('template', self.getSelfPageName(), contentPath)

    def highlightCurrentPage(self):
        self.output = []
        for line in self.contents:
            line = re.sub(r'(<a class=".*?)(" href=".*?">' + self.title + r'</a>)', r'\1 w3-deep-purple\2', line)
            self.output.append(line)
