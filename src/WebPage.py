"""
Responsible for generating a single page of the website

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

import re
from PseudoHtmlFile import PseudoHtmlFile
from NavTemplateFile import NavTemplateFile
from PageTemplateFile import PageTemplateFile


class WebPage:

    def __init__(self, outputDir, contentPath, templatePath=None):
        self.outputDir = outputDir
        self.contentPath = contentPath
        self.templatePath = templatePath

    def process(self, processMethod, write=True):
        # The object to which processMethod belongs (processMethod.__self__) is
        # the htmlFile to be read. Therefore, passing the htmlFile in as well
        # as the processMethod would be duplication. Therefore, obtain the
        # htmlFile from the processMethod as below.
        htmlFile = processMethod.__self__
        htmlFile.read()
        processMethod()
        if write:
            htmlFile.writeOutput()

    def processContent(self):
        contentFile = self.newContentFile(self.outputDir, self.contentPath)
        self.process(contentFile.parsePseudoHtml)

    def newContentFile(self, outputPath, contentPath):
        return PseudoHtmlFile(outputPath, contentPath)

    def processBar(self):
        self.processNav()

    def processDrop(self):
        self.processNav()

    def processNav(self):
        navFile = self.newNavFile(self.outputDir, self.templatePath, self.getTitle())
        self.process(navFile.highlightCurrentPage)

    def getTitle(self):
        filename = self.contentPath.split('/')[-1]
        title = re.sub(r'-content.shtml', '', filename)
        if title.islower():
            title = title.title()
        if title == 'Index':  # Deal with special case of index a.k.a. Home
            title = 'Home'
        if title == 'UKPSF':  # TODO - generalise later as this is project specific (config file?)
            title = 'Appendix - UKPSF'
        return title

    def newNavFile(self, outputDir, templatePath, title):
        return NavTemplateFile(outputDir, templatePath, title)

    def processPage(self):
        contentFile = self.newContentFile(self.outputDir, self.contentPath)
        self.process(contentFile.captureTocEntries, write=False)
        pageFile = self.newPageFile(self.outputDir, self.templatePath, self.getTitle(), contentFile.toc)
        self.process(pageFile.insertVariables)

    def newPageFile(self, outputDir, contentPath, title, toc):
        return PageTemplateFile(outputDir, contentPath, title, toc)
