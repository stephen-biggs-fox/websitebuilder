"""
Unit tests of HtmlTemplateFile

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import sys
sys.path.insert(0, '../src')
from HtmlTemplateFile import HtmlTemplateFile


class HtmlTemplateFileTest(TestCase):

    def setUp(self):
        self.outputDir = 'path/to/output'
        self.contentPath = 'path/to/file'
        self.title = 'title'

    def testHtmlTemplateFileStoresGivenPath(self):
        htf = HtmlTemplateFile(self.outputDir, self.contentPath, self.title)
        self.assertEqual(htf.contentPath, self.contentPath)

    def testHtmlTemplateFileStoresGivenTitle(self):
        htf = HtmlTemplateFile(self.outputDir, self.contentPath, self.title)
        self.assertEqual(htf.title, self.title)
