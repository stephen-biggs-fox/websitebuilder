"""
Unit tests of TocEntry

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from Toc import Toc


class TocTest(TestCase):

    def testGetHtmlReturnsHtmlOfEachTocEntrySeparatedByNewLines(self):
        firstEntryHtml = 'firstEntryHtml'
        secondEntryHtml = 'secondEntryHtml'
        thirdEntryHtml = 'thirdEntryHtml'
        firstEntry = mock.Mock()
        secondEntry = mock.Mock()
        thirdEntry = mock.Mock()
        firstEntry.getHtml = mock.Mock(return_value=firstEntryHtml)
        secondEntry.getHtml = mock.Mock(return_value=secondEntryHtml)
        thirdEntry.getHtml = mock.Mock(return_value=thirdEntryHtml)
        toc = Toc([firstEntry, secondEntry, thirdEntry])
        self.assertEqual(toc.getHtml(), firstEntryHtml + '\n' + secondEntryHtml + '\n' + thirdEntryHtml)

    def testGetHtmlReturnsHtmlOfEachAlternativeTocEntrySeparatedByNewLines(self):
        firstEntryHtml = 'alternativeFirstEntryHtml'
        secondEntryHtml = 'alternativeSecondEntryHtml'
        thirdEntryHtml = 'alternativeThirdEntryHtml'
        firstEntry = mock.Mock()
        secondEntry = mock.Mock()
        thirdEntry = mock.Mock()
        firstEntry.getHtml = mock.Mock(return_value=firstEntryHtml)
        secondEntry.getHtml = mock.Mock(return_value=secondEntryHtml)
        thirdEntry.getHtml = mock.Mock(return_value=thirdEntryHtml)
        toc = Toc([firstEntry, secondEntry, thirdEntry])
        self.assertEqual(toc.getHtml(), firstEntryHtml + '\n' + secondEntryHtml + '\n' + thirdEntryHtml)
