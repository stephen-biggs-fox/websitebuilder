"""
Unit tests of PageTemplateFile

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import operator
import sys
sys.path.insert(0, '../src')
from PageTemplateFile import PageTemplateFile

regularPageName = 'regular'
indexPageName = 'index'


class PageTemplateFileTest(TestCase):

    def setUp(self):
        self.outputDir = 'path/to/output'
        self.contentPath = 'path/to/file'
        self.title = 'LO2'
        self.mockToc = mock.Mock()
        self.tocText = 'tocLine1\ntocLine2\ntocLine3'
        self.mockToc.getHtml = mock.Mock(return_value=self.tocText)
        self.ntf = PageTemplateFile(self.outputDir, self.contentPath, self.title, self.mockToc)
        self.defineLines()
        self.ntf.contents = self.defineList()
        self.expectedContents = self.defineList()

    def testInitAssignsOutputFilenameAsOutputOfGetSelfFilename(self):
        ptf = MockPageTemplateFile(self.outputDir, self.contentPath, self.title, self.mockToc)
        self.assertEqual(ptf.outputFilename, regularPageName)

    def testInitAssignsOutputFilenameAsIndexDotHtmlNotJustIndex(self):
        ptf = MockHomePageTemplateFile(self.outputDir, self.contentPath, self.title, self.mockToc)
        self.assertEqual(ptf.outputFilename, indexPageName + '.html')

    def testPageTemplateFileStoresGivenPath(self):
        ptf = PageTemplateFile(self.outputDir, self.contentPath, self.title, self.mockToc)
        self.assertEqual(ptf.contentPath, self.contentPath)

    def testPageTemplateFileStoresGivenTitle(self):
        ptf = PageTemplateFile(self.outputDir, self.contentPath, self.title, self.mockToc)
        self.assertEqual(ptf.title, self.title)

    def testPageTemplateFileStoresGivenToc(self):
        ptf = PageTemplateFile(self.outputDir, self.contentPath, self.title, self.mockToc)
        self.assertEqual(ptf.toc, self.mockToc)

    def testInsertVariablesLeavesContentsUnchanged(self):
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.contents, self.expectedContents)

    def testInsertVariablesLeavesLinesWithoutReplaceStringUnchanged(self):
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[::2], self.expectedContents[::2])

    # TODO - generalise the remainder of these tests as these are project specific (config file?)

    def testInsertVariablesInsertsTitleIntoVarReplace1(self):
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[1], self.text + ' ' + self.title + ' ' + self.text)

    def testInsertVariablesInsertsBarPrevLineIntoVarReplace2(self):
        barPrevLine = 'barPrevLine'
        mockGetBarPrevLineMethod = mock.Mock(return_value=barPrevLine)
        self.ntf.getBarPreviousLine = mockGetBarPrevLineMethod
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[3], self.text + ' ' + barPrevLine + ' ' + self.text)

    def testGetPreviousReturnsPreviousForMidPage(self):
        self.assertEqual(self.ntf.getPreviousFilename(), 'LO1')

    def testGetPreviousReturnsPreviousForAnotherMidPage(self):
        self.ntf.title = 'LO3'
        self.assertEqual(self.ntf.getPreviousFilename(), 'LO2')

    def testGetPreviousReturnsNoneForFirstPage(self):
        self.ntf.title = 'Home'
        self.assertIsNone(self.ntf.getPreviousFilename())

    def testGetPreviousReturnsFilenameNotTitle(self):
        self.ntf.title = 'Bibliography'
        self.assertEquals(self.ntf.getPreviousFilename(), 'conclusions')

    def testGetPreviousReturnsIndexNotHome(self):
        self.ntf.title = 'Introduction'
        self.assertEquals(self.ntf.getPreviousFilename(), 'index')

    def testGetBarPrevLineReturnsCorrectBarPrevLineForMidPage(self):
        previous = 'LO1'
        self.ntf.getPreviousFilename = mock.Mock(return_value=previous)
        expected = '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small" href="' + previous + '">Previous</a>'
        actual = self.ntf.getBarPreviousLine()
        self.assertEqual(actual, expected)

    def testGetBarPrevLineReturnsCommentForFirstPage(self):
        self.ntf.title = 'Home'
        expected = '<!-- No previous on homepage -->'
        actual = self.ntf.getBarPreviousLine()
        self.assertEqual(actual, expected)

    def testInsertVariablesInsertsFilenameIntoVarReplace3(self):
        filename = 'filename'
        mockGetSelfFilenameMethod = mock.Mock(return_value=filename)
        self.ntf.getSelfPageName = mockGetSelfFilenameMethod
        self.ntf.insertVariables()
        actual = list(operator.itemgetter(5, 11, 19)(self.ntf.output))
        self.assertEqual(actual, 3 * [self.text + ' ' + filename + ' ' + self.text])

    def testInsertVariablesInsertsPageLinkAndTitleIntoVarReplace7And1OnTheSameLine(self):
        pageLink = 'pageLink'
        mockPageLinkMethod = mock.Mock(return_value=pageLink)
        self.ntf.getPageLink = mockPageLinkMethod
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[15], self.text + ' ' + pageLink + ' ' + self.text + ' ' + self.title + ' ' + self.text)

    def testGetPageLinkReturnsFilenameForGeneralPage(self):
        filename = 'filename'
        mockGetSelfFilenameMethod = mock.Mock(return_value=filename)
        self.ntf.getSelfPageName = mockGetSelfFilenameMethod
        self.assertEqual(self.ntf.getPageLink(), filename)

    def testGetPageLinkReturnsDotForHomePage(self):
        mockGetSelfFilenameMethod = mock.Mock(return_value='index')
        self.ntf.getSelfPageName = mockGetSelfFilenameMethod
        self.assertEqual(self.ntf.getPageLink(), '.')

    def testGetSelfFilenameCallsGetFilenameWithSelfTitle(self):
        mockGetFilenameMethod = mock.Mock()
        self.ntf.getPageName = mockGetFilenameMethod
        self.ntf.getSelfPageName()
        mockGetFilenameMethod.assert_called_with(self.ntf.title)

    def testGetSelfFilenameReturnsOutputFromGetFilename(self):
        filename = 'filename'
        mockGetFilenameMethod = mock.Mock(return_value=filename)
        self.ntf.getPageName = mockGetFilenameMethod
        self.assertEquals(self.ntf.getSelfPageName(), filename)

    def testGetFilenameReturnsTitleForLO2(self):
        actual = self.ntf.getPageName('LO2')
        self.assertEqual(actual, self.ntf.title)

    def testGetPageNameReturnsIndexForHome(self):
        actual = self.ntf.getPageName('Home')
        self.assertEqual(actual, 'index')

    def testGetFilenameReturnsCorrectFilenameForConclusions(self):
        actual = self.ntf.getPageName('Conclusions')
        self.assertEqual(actual, 'conclusions')

    def testGetFilenameReturnsCorrectFilenameForUkpsf(self):
        actual = self.ntf.getPageName('Appendix - UKPSF')
        self.assertEqual(actual, 'UKPSF')

    def testInsertVariablesInsertsBarNextLineIntoVarReplace3(self):
        barNextLine = 'barNextLine'
        mockGetBarNextLineMethod = mock.Mock(return_value=barNextLine)
        self.ntf.getBarNextLine = mockGetBarNextLineMethod
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[7], self.text + ' ' + barNextLine + ' ' + self.text)

    def testGetNextReturnsNextForMidPage(self):
        self.assertEqual(self.ntf.getNextFilename(), 'LO3')

    def testGetNextReturnsNextForAnotherMidPage(self):
        self.ntf.title = 'LO3'
        self.assertEqual(self.ntf.getNextFilename(), 'LO4')

    def testGetNextReturnsNoneForLastPage(self):
        self.ntf.title = 'Appendix - UKPSF'
        self.assertIsNone(self.ntf.getNextFilename())

    def testGetNextReturnsFilenameNotTitle(self):
        self.ntf.title = 'Bibliography'
        self.assertEquals(self.ntf.getNextFilename(), 'UKPSF')

    def testGetBarNextLineReturnsCorrectBarNextLineForMidPage(self):
        nextPage = 'LO3'
        self.ntf.getNextFilename = mock.Mock(return_value=nextPage)
        expected = '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small w3-hide-medium" href="' + nextPage + '">Next</a>'
        actual = self.ntf.getBarNextLine()
        self.assertEqual(actual, expected)

    def testGetBarNextLineReturnsCommentForLastPage(self):
        self.ntf.title = 'Appendix - UKPSF'
        expected = '<!-- No next on last page -->'
        actual = self.ntf.getBarNextLine()
        self.assertEqual(actual, expected)

    def testInsertVariablesInsertsDropPrevLineIntoVarReplace5(self):
        dropPrevLine = 'dropPrevLine'
        mockGetDropPrevLineMethod = mock.Mock(return_value=dropPrevLine)
        self.ntf.getDropPreviousLine = mockGetDropPrevLineMethod
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[9], self.text + ' ' + dropPrevLine + ' ' + self.text)

    def testGetDropPrevLineReturnsCorrectDropPrevLineForMidPage(self):
        previous = 'LO1'
        self.ntf.getPreviousFilename = mock.Mock(return_value=previous)
        expected = '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-medium" href="' + previous + '">Previous</a>'
        actual = self.ntf.getDropPreviousLine()
        self.assertEqual(actual, expected)

    def testGetDropPrevLineReturnsCommentForFirstPage(self):
        self.ntf.title = 'Home'
        expected = '<!-- No previous on homepage -->'
        actual = self.ntf.getDropPreviousLine()
        self.assertEqual(actual, expected)

    def testInsertVariablesInsertsDropNextLineIntoVarReplace6(self):
        dropNextLine = 'dropNextLine'
        mockGetDropNextLineMethod = mock.Mock(return_value=dropNextLine)
        self.ntf.getDropNextLine = mockGetDropNextLineMethod
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[13], self.text + ' ' + dropNextLine + ' ' + self.text)

    def testGetDropNextLineReturnsCorrectDropNextLineForMidPage(self):
        nextPage = 'LO3'
        self.ntf.getNextFilename = mock.Mock(return_value=nextPage)
        expected = '<a class="w3-bar-item w3-button w3-hover-orange" href="' + nextPage + '">Next</a>'
        actual = self.ntf.getDropNextLine()
        self.assertEqual(actual, expected)

    def testGetDropNextLineReturnsCommentForLastPage(self):
        self.ntf.title = 'Appendix - UKPSF'
        expected = '<!-- No next on last page -->'
        actual = self.ntf.getDropNextLine()
        self.assertEqual(actual, expected)

    def testInsertVariablesInsertsTocIntoVarReplace8(self):
        self.ntf.insertVariables()
        self.assertEqual(self.ntf.output[17], self.text + ' ' + self.tocText + ' ' + self.text)

    def defineLines(self):
        self.text = 'text'
        replace = 'VAR_REPLACE_'
        self.line0 = self.text
        self.line1 = self.text + ' ' + replace + '1' + ' ' + self.text
        self.line2 = self.text
        self.line3 = self.text + ' ' + replace + '2' + ' ' + self.text
        self.line4 = self.text
        self.line5 = self.text + ' ' + replace + '3' + ' ' + self.text
        self.line6 = self.text
        self.line7 = self.text + ' ' + replace + '4' + ' ' + self.text
        self.line8 = self.text
        self.line9 = self.text + ' ' + replace + '5' + ' ' + self.text
        self.line10 = self.text
        self.line11 = self.text + ' ' + replace + '3' + ' ' + self.text
        self.line12 = self.text
        self.line13 = self.text + ' ' + replace + '6' + ' ' + self.text
        self.line14 = self.text
        self.line15 = self.text + ' ' + replace + '7' + ' ' + self.text + ' ' + replace + '1' + ' ' + self.text
        self.line16 = self.text
        self.line17 = self.text + ' ' + replace + '8' + ' ' + self.text
        self.line18 = self.text
        self.line19 = self.text + ' ' + replace + '3' + ' ' + self.text
        self.line20 = self.text

    def defineList(self):
        return [self.line0,
            self.line1,
            self.line2,
            self.line3,
            self.line4,
            self.line5,
            self.line6,
            self.line7,
            self.line8,
            self.line9,
            self.line10,
            self.line11,
            self.line12,
            self.line13,
            self.line14,
            self.line15,
            self.line16,
            self.line17,
            self.line18,
            self.line19,
            self.line20]


class MockPageTemplateFile(PageTemplateFile):

    def getSelfPageName(self):
        return regularPageName


class MockHomePageTemplateFile(PageTemplateFile):

    def getSelfPageName(self):
        return indexPageName
