"""
Unit tests of PseudoHtmlBlock

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import sys
sys.path.insert(0, '../src')
from PseudoHtmlBlock import PseudoHtmlBlock as Block
from PseudoHtmlLine import PseudoHtmlLine as Line

paragraphOpeningTag = '<p>'
paragraphClosingTag = '</p>'


class PseudoHtmlBlockTest(TestCase):

    def testStartOfParagraphIsStartOfParagraph(self):
        block = Block(FakeBlankLine(), FakeParagraphLine(),
                      FakeParagraphLine())
        self.assertTrue(block.isStartOfParagraph())

    def testBlankIsNotStartOfParagraph(self):
        block = Block(FakeBlankLine(), FakeBlankLine(),
                      FakeBlankLine())
        self.assertFalse(block.isStartOfParagraph())

    def testHeaderIsNotStartOfParagraph(self):
        block = Block(FakeBlankLine(), FakeHeaderLine(),
                      FakeBlankLine())
        self.assertFalse(block.isStartOfParagraph())

    def testListIsNotStartOfParagraph(self):
        block = Block(FakeBlankLine(), FakeListLine(),
                      FakeBlankLine())
        self.assertFalse(block.isStartOfParagraph())

    def testMiddleOfParagraphIsNotStartOfParagraph(self):
        block = Block(FakeParagraphLine(), FakeParagraphLine(),
                      FakeParagraphLine())
        self.assertFalse(block.isStartOfParagraph())

    def testOneLineParagraphIsStartOfParagraph(self):
        block = Block(FakeBlankLine(), FakeParagraphLine(),
                      FakeBlankLine())
        self.assertTrue(block.isStartOfParagraph())

    def testStartOfParagraphGetsParagraphOpened(self):
        block = FakeStartOfParagraphBlock()
        block.parsePseudoHtml()
        self.assertTrue(block.this.wasOpened)

    def testBlankLineRemainsUnchanged(self):
        block = FakeBlankLineBlock()
        line = FakeLine()
        block.this = line
        actual = block.parsePseudoHtml()
        self.assertEqual(actual, line)

    def testNotStartOfParagraphDoesNotGetOpened(self):
        block = FakeBlankLineBlock()
        block.parsePseudoHtml()
        self.assertFalse(block.this.wasOpened)

    def testEndOfParagraphIsEndOfParagraph(self):
        block = Block(FakeParagraphLine(), FakeParagraphLine(),
                      FakeBlankLine())
        self.assertTrue(block.isEndOfParagraph())

    def testBlankIsNotEndOfParagraph(self):
        block = Block(FakeBlankLine(), FakeBlankLine(),
                      FakeBlankLine())
        self.assertFalse(block.isEndOfParagraph())

    def testHeaderIsNotEndOfParagraph(self):
        block = Block(FakeBlankLine(), FakeHeaderLine(),
                      FakeBlankLine())
        self.assertFalse(block.isEndOfParagraph())

    def testListIsNotEndOfParagraph(self):
        block = Block(FakeBlankLine(), FakeListLine(),
                      FakeBlankLine())
        self.assertFalse(block.isEndOfParagraph())

    def testMiddleOfParagraphIsNotEndOfParagraph(self):
        block = Block(FakeParagraphLine(), FakeParagraphLine(),
                      FakeParagraphLine())
        self.assertFalse(block.isEndOfParagraph())

    def testOneLineParagraphIsEndOfParagraph(self):
        block = Block(FakeBlankLine(), FakeParagraphLine(),
                      FakeBlankLine())
        self.assertTrue(block.isEndOfParagraph())

    def testEndOfParagraphGetsClosed(self):
        block = FakeEndOfParagraphBlock()
        block.parsePseudoHtml()
        self.assertTrue(block.this.wasClosed)

    def testNotEndOfParagraphDoesNotGetClosed(self):
        block = FakeBlankLineBlock()
        block.parsePseudoHtml()
        self.assertFalse(block.this.wasClosed)

    def testOneLineParagraphGetsOpened(self):
        block = FakeOneLineParagraphBlock()
        block.parsePseudoHtml()
        self.assertTrue(block.this.wasOpened)

    def testOneLineParagraphGetsClosed(self):
        block = FakeOneLineParagraphBlock()
        block.parsePseudoHtml()
        self.assertTrue(block.this.wasClosed)

    def testBookmarkGetsFixed(self):
        block = FakeBookmarkBlock()
        block.parsePseudoHtml()
        self.assertTrue(block.this.wasFixed)

    def testBookmarkIsBookmark(self):
        block = Block(FakeBlankLine(), FakeBookmarkLine(),
                      FakeBlankLine())
        self.assertTrue(block.isBookmark())

    def testBlankIsNotBookmark(self):
        block = Block(FakeBlankLine(), FakeBlankLine(),
                      FakeBlankLine())
        self.assertFalse(block.isBookmark())

    def testParagraphIsNotBookmark(self):
        block = Block(FakeBlankLine(), FakeParagraphLine(),
                      FakeBlankLine())
        self.assertFalse(block.isBookmark())

    def testBlockInitialisedWithNoParametersHasBeforeAsBlankLine(self):
        block = Block()
        self.assertEqual(block.before, Line())

    def testBlockInitialisedWithNoParametersHasThisAsNone(self):
        block = Block()
        self.assertEqual(block.this, Line())

    def testBlockInitialisedWithNoParametersHasAfterAsNone(self):
        block = Block()
        self.assertEqual(block.after, Line())


class PseudoHtmlBlockStepTest(TestCase):

    def setUp(self):
        self.oldBefore = FakeLine()
        self.oldThis = FakeLine()
        self.oldAfter = FakeLine()
        self.newAfter = FakeLine()
        self.block = Block(self.oldBefore, self.oldThis,
                           self.oldAfter)

    def testStepMovesOldThisToNewBefore(self):
        self.block.step(self.newAfter)
        self.assertIs(self.block.before, self.oldThis)

    def testStepMovesOldAfterToNewThis(self):
        self.block.step(self.newAfter)
        self.assertIs(self.block.this, self.oldAfter)

    def testStepMovesNewAfterToNewAfter(self):
        self.block.step(self.newAfter)
        self.assertIs(self.block.after, self.newAfter)


class FakeStartOfParagraphBlock(Block):

    def __init__(self):
        self.this = FakeLine()

    def isStartOfParagraph(self):
        return True

    def isEndOfParagraph(self):
        return False

    def isBookmark(self):
        return False


class FakeBlankLineBlock(Block):

    def __init__(self):
        self.this = FakeLine()

    def isStartOfParagraph(self):
        return False

    def isEndOfParagraph(self):
        return False

    def isBookmark(self):
        return False


class FakeEndOfParagraphBlock(Block):

    def __init__(self):
        self.this = FakeLine()

    def isStartOfParagraph(self):
        return False

    def isEndOfParagraph(self):
        return True

    def isBookmark(self):
        return False


class FakeOneLineParagraphBlock(Block):

    def __init__(self):
        self.this = FakeLine()

    def isStartOfParagraph(self):
        return True

    def isEndOfParagraph(self):
        return True

    def isBookmark(self):
        return False


class FakeBookmarkBlock(Block):

    def __init__(self):
        self.this = FakeLine()

    def isStartOfParagraph(self):
        return False

    def isEndOfParagraph(self):
        return False

    def isBookmark(self):
        return True


class FakeLine(Line):

    def __init__(self):
        super(FakeLine, self).__init__('dummy')
        self.wasOpened = False
        self.wasClosed = False
        self.wasFixed = False

    def openParagraph(self):
        self.wasOpened = True
        return self

    def closeParagraph(self):
        self.wasClosed = True

    def formatBookmark(self):
        self.wasFixed = True


class FakeBlankLine(Line):

    def __init__(self):
        super(FakeBlankLine, self).__init__('blank')

    def isBlank(self):
        return True

    def isHeader(self):
        return False

    def isBookmark(self):
        return False

    def isList(self):
        return False


class FakeParagraphLine(Line):

    def __init__(self):
        super(FakeParagraphLine, self).__init__('para')

    def isBlank(self):
        return False

    def isHeader(self):
        return False

    def isBookmark(self):
        return False

    def isList(self):
        return False


class FakeHeaderLine(Line):

    def __init__(self):
        super(FakeHeaderLine, self).__init__('head')

    def isBlank(self):
        return False

    def isHeader(self):
        return True

    def isBookmark(self):
        return False

    def isList(self):
        return False


class FakeListLine(Line):

    def __init__(self):
        super(FakeListLine, self).__init__('list')

    def isBlank(self):
        return False

    def isHeader(self):
        return False

    def isBookmark(self):
        return False

    def isList(self):
        return True


class FakeBookmarkLine(Line):

    def __init__(self):
        super(FakeBookmarkLine, self).__init__('book')
        self.wasFixed = False

    def isBlank(self):
        return False

    def isHeader(self):
        return False

    def isBookmark(self):
        return True

    def isList(self):
        return False

    def fixBookmarkLine(self):
        self.wasFixed = True
