"""
Unit tests of PseudoHtmlFile

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from HtmlFile import HtmlFile
from PseudoHtmlLine import PseudoHtmlLine as Line


class HtmlFileTest(TestCase):

    def setUp(self):
        self.outputDir = 'path/to/output'
        self.contentPath = 'path/to/file'
        self.htmlFile = HtmlFile(self.outputDir, self.contentPath)
        self.lineText = "first line\n"
        self.fileContents = [self.lineText]
        self.expectedOutputText = "output"
        self.htmlFile.output = [self.expectedOutputText]
        self.htmlFile.outputFilename = 'outputFilename'

    def testInitHoldsOntoGivenOutputDir(self):
        self.assertEqual(self.htmlFile.outputDir, self.outputDir)

    def testInitHoldsOntoGivenContentFilename(self):
        self.assertEqual(self.htmlFile.contentPath, self.contentPath)

    def testContentsIsNoneBeforeFileIsRead(self):
        self.assertIsNone(self.htmlFile.contents)

    def testReadOpensFileReadonly(self):
        mockOpenFunction = self.mockOpenFunction()
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.read()
            mockOpenFunction.assert_called_with(self.contentPath, 'r')

    def testReadClosesFile(self):
        # Use a MagicMock to access the __exit__ attribute
        mockFileObject = mock.MagicMock()
        mockOpenFunction = mock.Mock(return_value=mockFileObject)
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.read()
        self.assertTrue(mockFileObject.close.called or
                        mockFileObject.__exit__.called)

    def testReadPopulatesContentsWithContentsOfAOneLineFile(self):
        mockOpenFunction = self.mockOpenFunction()
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.read()
        self.assertEqual(self.htmlFile.contents, self.fileContents)

    def testReadPopulatesContentsWithContentsOfAManyLineFile(self):
        self.fileContents = ["first line\n", "second line\n", "etc\n"]
        mockOpenFunction = self.mockOpenFunction()
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.read()
        self.assertEqual(self.htmlFile.contents, self.fileContents)

    def testWriteOutputOpensOutputFileForWriting(self):
        mockOpenFunction = self.mockOpenFunction()
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.writeOutput()
            mockOpenFunction.assert_called_with(
                self.htmlFile.outputDir + '/' + self.htmlFile.outputFilename, 'w')

    def testWriteOutputClosesOutputFile(self):
        # Use a MagicMock to access the __exit__ attribute
        mockFileObject = mock.MagicMock()
        mockOpenFunction = mock.Mock(return_value=mockFileObject)
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.writeOutput()
        self.assertTrue(mockFileObject.close.called or
                        mockFileObject.__exit__.called)

    def testWriteOutputWritesOneLineOutputToFile(self):
        mockOpenFunction = self.mockOpenFunction()
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.writeOutput()
        mockFileObject = mockOpenFunction()
        mockFileObject.write.assert_called_with(self.expectedOutputText)

    def testWriteOutputWritesToFileForEachLine(self):
        mockFirstLine = mock.Mock(spec=Line)
        mockFirstLine.text = "first output"
        mockSecondLine = mock.Mock(spec=Line)
        mockSecondLine.text = "second output"
        self.htmlFile.output = [mockFirstLine, mockSecondLine]
        mockOpenFunction = self.mockOpenFunction()
        with mock.patch("builtins.open", mockOpenFunction):
            self.htmlFile.writeOutput()
        mockFileObject = mockOpenFunction()
        self.assertEqual(mockFileObject.write.call_count,
                         len(self.htmlFile.output))

    def mockOpenFunction(self):
        mockOpenFunction = mock.mock_open(
            read_data=''.join(self.fileContents))
        mockOpenFunction.return_value.__iter__ = lambda self: self
        mockOpenFunction.return_value.__next__ = lambda self: next(
            iter(self.readline, ''))
        return mockOpenFunction
