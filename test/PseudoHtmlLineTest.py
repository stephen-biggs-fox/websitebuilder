"""
Unit tests of PseudoHtmlLine

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from PseudoHtmlLine import PseudoHtmlLine as Line


class PseudoHtmlLineTest(TestCase):

    def setUp(self):
        self.bookmarkText = '<h1><a name="title">Title</a></h1>\n'
        self.paragraphText = 'blah\n'

    def testTocEntryIsNoneBeforeCapturing(self):
        line = Line()
        self.assertIsNone(line.tocEntry)

    def testTocEntryIsStillNoneAfterCapturingTocEntryFromNonBookmarkLine(self):
        line = Line()
        line.isBookmark = mock.Mock(return_value=False)
        line.captureTocEntry()
        self.assertIsNone(line.tocEntry)

    def testBlankLineIsBlank(self):
        line = Line('')
        self.assertTrue(line.isBlank())

    def testLineWithNewlineCharacterOnlyIsBlank(self):
        line = Line('\n')
        self.assertTrue(line.isBlank())

    def testNotBlankLineIsNotBlank(self):
        line = Line(self.paragraphText)
        self.assertFalse(line.isBlank())

    def testNoneIsBlank(self):
        line = Line(None)
        self.assertTrue(line.isBlank())

    def testHeaderLineIsHeader(self):
        line = Line(self.bookmarkText)
        self.assertTrue(line.isHeader())

    def testNonHeaderLineIsNotHeader(self):
        line = Line(self.paragraphText)
        self.assertFalse(line.isHeader())

    def testBookmarkLineIsBookmark(self):
        line = Line(self.bookmarkText)
        self.assertTrue(line.isBookmark())

    def testRegularLinkIsNotBookmark(self):
        line = Line('<a href=".">regular link</a>')
        self.assertFalse(line.isBookmark())

    def testNoneIsNotBookmark(self):
        line = Line(None)
        self.assertFalse(line.isBookmark())

    def testInsertFormattingClassesInsertsFormattingClasses(self):
        expected = '<h1><a class="anchor w3-text-black" name="title">Title</a></h1>\n'
        actual = Line().insertFormattingClasses(self.bookmarkText)
        self.assertEqual(actual, expected)

    def testRemoveTocAttributeRemovesTocAttribute(self):
        expected = '<h1><a name="title">Title</a></h1>\n'
        actual = Line().removeTocAttribute('<h1><a name="title" toc="label">Title</a></h1>\n')
        self.assertEqual(actual, expected)

    def testCaptureTocEntryH1(self):
        line = Line('<h1><a name="title" toc="label">Title</a></h1>\n')
        line.isBookmark = mock.Mock()
        line.isBookmark.return_value = True
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.level, 1)

    def testCaptureTocEntryTitle(self):
        line = Line('<h1><a name="title" toc="label">Title</a></h1>\n')
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.anchor, 'title')

    def testCaptureTocEntryLabel(self):
        line = Line('<h1><a name="title" toc="label">Title</a></h1>\n')
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.label, 'label')

    def testCaptureTocEntryH2(self):
        line = Line('<h2><a name="anchor" toc="entry">Title</a></h2>\n')
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.level, 2)

    def testCaptureTocEntryAnchor(self):
        line = Line('<h2><a name="anchor" toc="entry">Title</a></h2>\n')
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.anchor, 'anchor')

    def testCaptureTocEntryEntry(self):
        line = Line('<h2><a name="anchor" toc="entry">Title</a></h2>\n')
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.label, 'entry')

    def testCaptureTocEntryAnchorSingle(self):
        line = Line("<h2><a name='anchor' toc='entry'>Title</a></h2>\n")
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.anchor, 'anchor')

    def testCaptureTocEntryEntrySingle(self):
        line = Line("<h2><a name='anchor' toc='entry'>Title</a></h2>\n")
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.label, 'entry')

    def testCaptureTocEntryUnspecified(self):
        line = Line("<h2><a name='anchor'>Title</a></h2>\n")
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertEqual(line.tocEntry.label, 'Title')

    def testTocEntryIsStillNoneAfterCapturingTocEntryFromTitleLine(self):
        line = Line("<h1><a name='top'>Title</a></h1>\n")  # top is a special name
        line.isBookmark = mock.Mock(return_value=True)
        line.captureTocEntry()
        self.assertIsNone(line.tocEntry)

    def testProcessBookmarkReturnsANewObject(self):
        line = Line(self.bookmarkText)
        actual = line.formatBookmark()
        self.assertIsNot(actual, line)

    def testTwoLinesAreEqualIfTheyHaveTheSameText(self):
        self.assertEqual(Line('same text'), Line('same text'))

    def testTwoLinesAreNotEqualIfTheyHaveDifferentText(self):
        self.assertNotEqual(Line('same text'), Line('different text'))

    def testTwoLinesAreEqualIfTheyHaveTheSameTextAndOneIsASubclass(self):
        self.assertEqual(Line('same text'), FakeLine('same text'))

    def testTwoLinesAreNotEqualIfTheyHaveDifferentTextAndOneIsASubclass(self):
        self.assertNotEqual(Line('same text'), FakeLine('different text'))

    def testTwoLinesAreEqualSameTextSubclassOppositeOrder(self):
        self.assertEqual(FakeLine('same text'), Line('same text'))

    def testTwoLinesAreNotEqualDifferentTextSubclassOppositeOrder(self):
        self.assertNotEqual(FakeLine('same text'), Line('different text'))

    def testOpenParagraphPrependsParagraphOpeningTag(self):
        line = Line(self.paragraphText)
        actual = line.openParagraph()
        self.assertEqual(actual, Line('<p>' + self.paragraphText))

    def testOpenParagraphReturnsANewObject(self):
        line = Line(self.paragraphText)
        actual = line.openParagraph()
        self.assertIsNot(actual, line)

    def testCloseParagraphAppendsParagraphClosingTag(self):
        paragraphText = 'blah'
        line = Line(paragraphText)
        actual = line.closeParagraph()
        self.assertEqual(actual, Line(paragraphText + '</p>'))

    def testCloseParagraphInsertsParagraphClosingTagBeforeNewlineCharacter(self):
        paragraphText = 'blah'
        newline = '\n'
        line = Line(paragraphText + newline)
        actual = line.closeParagraph()
        self.assertEqual(actual, Line(paragraphText + '</p>' + newline))

    def testCloseParagraphReturnsANewObject(self):
        line = Line(self.paragraphText)
        actual = line.closeParagraph()
        self.assertIsNot(actual, line)

    def testLineInitialisedWithNoParametersIsABlankLine(self):
        self.assertEqual(Line(), Line(''))

    def testStrLineReturnsText(self):
        text = "expected"
        expected = "PseudoHtmlLine('" + text + "')"
        self.assertEqual(str(Line(text)), expected)

    def testReprLineReturnsText(self):
        text = "expected"
        expected = "PseudoHtmlLine('" + text + "')"
        self.assertEqual(repr(Line(text)), expected)

    def testParagraphIsNotList(self):
        line = Line(self.paragraphText)
        self.assertFalse(line.isList())

    def testOrderedListIsList(self):
        line = Line('<ol>\n')
        self.assertTrue(line.isList())

    def testOrderedListCloseIsList(self):
        line = Line('</ol>\n')
        self.assertTrue(line.isList())

    def testUnorderedListIsList(self):
        line = Line('<ul>\n')
        self.assertTrue(line.isList())

    def testUnorderedListCloseIsList(self):
        line = Line('</ul>\n')
        self.assertTrue(line.isList())


class PseudoHtmlLineFormatBookmarkTest(TestCase):

    def setUp(self):
        self.originalText = "original"
        self.line = Line(self.originalText)
        self.modifiedText = "modified"
        self.mockInsertFormattingClassesMethod = mock.Mock(return_value=self.modifiedText)
        self.line.insertFormattingClasses = self.mockInsertFormattingClassesMethod
        self.mockRemoveTocAttributeMethod = mock.Mock()
        self.line.removeTocAttribute = self.mockRemoveTocAttributeMethod

    def testFormatBookmarkInsertsClassesIntoOriginalText(self):
        self.line.formatBookmark()
        self.mockInsertFormattingClassesMethod.assert_called_once_with(self.originalText)

    def testFormatBookmarkRemovesTocFromModifiedText(self):
        self.line.formatBookmark()
        self.mockRemoveTocAttributeMethod.assert_called_once_with(self.modifiedText)


class FakeLine(Line):
    pass
