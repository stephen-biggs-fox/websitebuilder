"""
Unit tests of NavTemplateFile

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import sys
sys.path.insert(0, '../src')
from NavTemplateFile import NavTemplateFile


class NavTemplateFileTest(TestCase):

    def setUp(self):
        self.outputDir = 'path/to/output'
        self.contentPath = 'path/to/file'
        self.title = 'LO2'
        self.ntf = NavTemplateFile(self.outputDir, self.contentPath, self.title)
        self.firstLine = '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small" href="LO1">LO1</a>'
        self.secondLine = '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small" href="LO2">LO2</a>'
        self.thirdLine = '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small" href="LO3">LO3</a>'
        self.ntf.contents = [
            self.firstLine,
            self.secondLine,
            self.thirdLine]

    def testInitAssignsOutputFilenameCorrectlyForBar(self):
        ntf = NavTemplateFile(self.outputDir, 'nav-bar-template.html', self.title)
        self.assertEqual(ntf.outputFilename, 'nav-bar-' + self.title + '.html')

    def testInitAssignsOutputFilenameCorrectlyForAnotherBar(self):
        title = 'LO3'
        ntf = NavTemplateFile(self.outputDir, 'nav-bar-template.html', title)
        self.assertEqual(ntf.outputFilename, 'nav-bar-' + title + '.html')

    def testInitAssignsOutputFilenameCorrectlyForHomePage(self):
        ntf = NavTemplateFile(self.outputDir, 'nav-bar-template.html', 'Home')
        self.assertEqual(ntf.outputFilename, 'nav-bar-index.html')

    def testInitAssignsOutputFilenameCorrectlyForDrop(self):
        ntf = NavTemplateFile(self.outputDir, 'nav-drop-template.html', self.title)
        self.assertEqual(ntf.outputFilename, 'nav-drop-' + self.title + '.html')

    def testNavTemplateFileStoresGivenPath(self):
        self.assertEqual(self.ntf.contentPath, self.contentPath)

    def testNavTemplateFileStoresGivenTitle(self):
        self.assertEqual(self.ntf.title, self.title)

    def testHighlightCurrentPageLeavesFirstLineOfContentsUnchanged(self):
        self.ntf.highlightCurrentPage()
        self.assertEqual(self.ntf.contents[0], self.firstLine)

    def testHighlightCurrentPageLeavesSecondLineOfContentsUnchanged(self):
        self.ntf.highlightCurrentPage()
        self.assertEqual(self.ntf.contents[1], self.secondLine)

    def testHighlightCurrentPageLeavesThirdLineOfContentsUnchanged(self):
        self.ntf.highlightCurrentPage()
        self.assertEqual(self.ntf.contents[2], self.thirdLine)

    def testHighlightCurrentPageLeavesPageBeforeUnchanged(self):
        self.ntf.highlightCurrentPage()
        self.assertEqual(self.ntf.output[0], self.firstLine)

    def testHighlightCurrentPageLeavesPageAfterUnchanged(self):
        self.ntf.highlightCurrentPage()
        self.assertEqual(self.ntf.output[2], self.thirdLine)

    def testHighlightCurrentPageInsertsHighlightClassIntoCurrentPageLine(self):
        expected = '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small w3-deep-purple" href="LO2">LO2</a>'
        self.ntf.highlightCurrentPage()
        self.assertEqual(self.ntf.output[1], expected)
