"""
Unit tests of WebPage

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
from PageTemplateFile import PageTemplateFile
from PseudoHtmlFile import PseudoHtmlFile
from NavTemplateFile import NavTemplateFile
sys.path.insert(0, '../src')
from WebPage import WebPage as Page
from HtmlFile import HtmlFile


class WebPageTest(TestCase):

    def setUp(self):
        self.outputDir = 'path/to/output'
        self.contentPath = 'path/to/content'
        self.templatePath = 'path/to/template'

    def testPageStoresFirstArgAsOutputPath(self):
        page = Page(self.outputDir, self.contentPath)
        self.assertEqual(page.outputDir, self.outputDir)

    def testPageStoresSecondArgAsContentPath(self):
        page = Page(self.outputDir, self.contentPath)
        self.assertEqual(page.contentPath, self.contentPath)

    def testWebPageInitialisedWithTwoArgsHasTemplateFileAsNone(self):
        page = Page(self.outputDir, self.contentPath)
        self.assertIsNone(page.templatePath)

    def testPageStoresThirdArgAsTemplatePath(self):
        page = Page(self.outputDir, self.contentPath, self.templatePath)
        self.assertEqual(page.templatePath, self.templatePath)

    def testPageStoresSecondArgAsContentPathWhenInitialisedUsingAList(self):
        args = [self.contentPath, self.templatePath]
        page = Page(self.outputDir, *args)
        self.assertEqual(page.contentPath, self.contentPath)

    def testPageStoresThirdArgAsTemplatePathWhenInitialisedUsingAList(self):
        args = [self.contentPath, self.templatePath]
        page = Page(self.outputDir, *args)
        self.assertEqual(page.templatePath, self.templatePath)

    def testGetTitleReturnsCorrectTitleForSimpleCase(self):
        title = 'Page'
        page = Page(self.outputDir, title + '-content.shtml')
        self.assertEqual(page.getTitle(), title)

    def testGetTitleReturnsCorrectTitleForAnotherSimpleCase(self):
        title = 'Another'
        page = Page(self.outputDir, title + '-content.shtml')
        self.assertEqual(page.getTitle(), title)

    def testGetTitleReturnsCorrectTitleForFileInSubdirectory(self):
        title = 'Page'
        page = Page(self.outputDir, 'path/to/' + title + '-content.shtml')
        self.assertEqual(page.getTitle(), title)

    def testGetTitleReturnsCorrectTitleForIndexAkaHome(self):
        page = Page(self.outputDir, 'index-content.shtml')
        self.assertEqual(page.getTitle(), 'Home')

    def testGetTitleCapitalisesLowerCaseTitles(self):
        page = Page(self.outputDir, 'summary-content.shtml')
        self.assertEqual(page.getTitle(), 'Summary')

    def testGetTitlePrependsAppendixToUkpsf(self):
        # TODO - generalise later as this is project specific (config file?)
        page = Page(self.outputDir, 'UKPSF-content.shtml')
        self.assertEqual(page.getTitle(), 'Appendix - UKPSF')

    def testGetTitleMaintainsAllCaps(self):
        page = Page(self.outputDir, 'LO2-content.shtml')
        self.assertEqual(page.getTitle(), 'LO2')


class WebPageProcessMethodsTest(TestCase):

    def setUp(self):
        self.outputDir = 'path/to/output'
        self.contentPath = 'path/to/content'
        self.templatePath = 'path/to/template'
        self.page = Page(self.outputDir, self.contentPath, self.templatePath)
        # Content
        self.mockContentFile = MockHtmlFile()
        self.mockContentFile.toc = mock.Mock()
        self.mockNewContentFileMethod = mock.Mock(return_value=self.mockContentFile)
        self.page.newContentFile = self.mockNewContentFileMethod
        # Nav
        self.mockNavFile = MockHtmlFile()
        self.mockNewNavFileMethod = mock.Mock(return_value=self.mockNavFile)
        self.page.newNavFile = self.mockNewNavFileMethod
        # Page
        self.mockPageFile = MockHtmlFile()
        self.mockNewPageFileMethod = mock.Mock(return_value=self.mockPageFile)
        self.page.newPageFile = self.mockNewPageFileMethod
        # Title
        self.mockTitle = mock.Mock()
        self.mockGetTitleMethod = mock.Mock(return_value=self.mockTitle)
        self.page.getTitle = self.mockGetTitleMethod

    def testProcessCallsReadFirst(self):
        self.page.process(self.mockContentFile.processMethod)
        self.assertEqual(self.mockContentFile.readCallIndexes, [0])

    def testProcessCallsGivenMethodSecond(self):
        self.page.process(self.mockContentFile.processMethod)
        self.assertEqual(self.mockContentFile.processMethodCallIndexes, [1])

    def testProcessCallsWriteHtmlOutputThird(self):
        self.page.process(self.mockContentFile.processMethod)
        self.assertEqual(self.mockContentFile.writeHtmlOutputCallIndexes, [2])

    def testProcessDoesNotCallWriteHtmlOutputWhenWriteEqualsFalse(self):
        self.page.process(self.mockContentFile.processMethod, write=False)
        self.assertEqual(self.mockContentFile.writeHtmlOutputCallIndexes, [])

    def testProcessContentCallsNewContentFileCorrectly(self):
        self.page.processContent()
        self.mockNewContentFileMethod.assert_called_with(self.outputDir, self.contentPath)

    def testProcessContentCallsProcessWithNewContentFileAndItsParsePseudoHtmlMethod(self):
        mockProcessMethod = mock.Mock()
        self.page.process = mockProcessMethod
        self.page.processContent()
        mockProcessMethod.assert_called_with(self.mockContentFile.parsePseudoHtml)

    def testProcessBarDelegatesToProcessNav(self):
        mockProcessNavMethod = mock.Mock()
        self.page.processNav = mockProcessNavMethod
        self.page.processBar()
        self.assertTrue(mockProcessNavMethod.called)

    def testProcessDropDelegatesToProcessNav(self):
        mockProcessNavMethod = mock.Mock()
        self.page.processNav = mockProcessNavMethod
        self.page.processDrop()
        self.assertTrue(mockProcessNavMethod.called)

    def testProcessNavCallsNewNavFileCorrectly(self):
        self.page.processNav()
        self.mockNewNavFileMethod.assert_called_with(self.outputDir, self.templatePath, self.mockTitle)

    def testProcessNavCallsProcessWithNewNavFileAndItsHighlightCurrentPageMethod(self):
        mockProcessMethod = mock.Mock()
        self.page.process = mockProcessMethod
        self.page.processNav()
        mockProcessMethod.assert_called_with(self.mockNavFile.highlightCurrentPage)

    def testProcessPageCallsNewContentFileCorrectly(self):
        self.page.processPage()
        self.mockNewContentFileMethod.assert_called_with(self.outputDir, self.contentPath)

    def testProcessPageCallsProcessWithNewContentFileCaptureTocEntriesThenWithNewPageFileInsertVariables(self):
        mockProcessMethod = mock.Mock()
        self.page.process = mockProcessMethod
        expectedCalls = [
            mock.call(self.mockContentFile.captureTocEntries, write=False),
            mock.call(self.mockPageFile.insertVariables)
            ]
        self.page.processPage()
        mockProcessMethod.assert_has_calls(expectedCalls)

    def testProcessPageCallsNewPageFileCorrectly(self):
        title = 'title'
        self.page.getTitle = mock.Mock(return_value=title)
        self.page.processPage()
        self.mockNewPageFileMethod.assert_called_with(self.outputDir, self.templatePath, title, self.mockContentFile.toc)

    def testNewContentFileReturnsAContentFile(self):
        outputPath = 'path/to/output'
        contentPath = 'path/to/content'
        page = Page(outputPath, contentPath)
        actual = page.newContentFile(outputPath, contentPath)
        self.assertIsInstance(actual, PseudoHtmlFile)

    def testNewNavFileReturnsANavFile(self):
        page = Page(self.outputDir, self.contentPath)
        actual = page.newNavFile(self.outputDir, self.page.templatePath, 'title')
        self.assertIsInstance(actual, NavTemplateFile)

    def testNewPageFileReturnsAPageFile(self):
        page = Page(self.outputDir, self.contentPath)
        actual = page.newPageFile(self.outputDir, self.page.templatePath, 'title', self.mockContentFile.toc)
        self.assertIsInstance(actual, PageTemplateFile)


class MockHtmlFile(HtmlFile):

    def __init__(self):
        super(MockHtmlFile, self).__init__('path/to/output', 'path/to/file')
        self.callCount = 0
        self.readCallIndexes = []
        self.processMethodCallIndexes = []
        self.writeHtmlOutputCallIndexes = []

    def read(self):
        self.readCallIndexes.append(self.callCount)
        self.callCount += 1

    def processMethod(self):
        self.processMethodCallIndexes.append(self.callCount)
        self.callCount += 1

    def writeOutput(self):
        self.writeHtmlOutputCallIndexes.append(self.callCount)
        self.callCount += 1

    def parsePseudoHtml(self):
        pass

    def captureTocEntries(self):
        pass

    def highlightCurrentPage(self):
        pass

    def insertVariables(self):
        pass
